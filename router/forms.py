from django import forms
from router.models import RouterInfo

class RouterInfoAdminForm(forms.ModelForm):
    class Meta:
        model = RouterInfo
        fields = "__all__"


