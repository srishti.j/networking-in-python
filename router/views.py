import os
import re
from paramiko import SSHClient, AutoAddPolicy
from django.shortcuts import render
from django.http import HttpResponse
from router.models import RouterInfo
from router.admin import RouterInfoAdmin

def add_router_info(request):

	router_data = RouterInfo.objects.get(id=13)
	router_dict={
		"interface":router_data.interface,
		"description":router_data.description,
		"bandwidth":str(router_data.bandwidth),
		"IP Address":router_data.ip_address+' '+router_data.gateway,
		}
	ssh_client = SSHClient()
	ssh_client.set_missing_host_key_policy(AutoAddPolicy())
	ssh_client.connect(hostname='35.197.43.130',username='srishti',sock=None,key_filename='/home/ebabu/Downloads/test_box')
	sftp = ssh_client.open_sftp()
	localpath = '/home/ebabu/Desktop/test22_file.txt'
	remotepath = '/home/srishti/wan.conf'
	sftp.get(remotepath,localpath)
	
	elist = []
	with open(localpath,"r") as f:
		contents = f.readlines()
		f.close()
	for position,line in enumerate(contents):
		if line.startswith("interface") or line.startswith("#"):
			lnum = re.findall('\d+',line.split()[1])
			elist.append(lnum)
			enum = re.findall('\d+',router_data.interface)

			if lnum:
				if lnum >= enum:
					try:
						elist.append(enum)
						count = 0
						while count<4:
							for i in router_dict.keys():
								if count>0:
									contents.insert(position+count,' {} {}\n'.format(i,router_dict[i]))
								else:
									contents.insert(position,'{} {}\n'.format(i,router_dict[i]))
								count+=1
							with open(localpath,"w") as f:
								contents = "".join(contents)
								f.write(contents)
								f.close()
							break
					except:
						pass
				

	with open(localpath,"r") as f:
		contents = f.readlines()
		f.close()
	if enum not in elist:
		for position,line in enumerate(contents):
			if line == 'end\n':
				try:
					count=0
					while count<4:
						for i in router_dict:
							if count>0:
								contents.insert(position+count,' {} {} \n'.format(i,router_dict[i]))
							else:
								contents.insert(position,'{} {} \n'.format(i,router_dict[i]))
							count+=1
						with open(localpath,"w") as f:
							contents = "".join(contents)
							f.write(contents)
							f.close()
						break
				except:
					pass


	path = '/home/srishti/wan3.conf'
	sftp.put(localpath,path)	
	sftp.close()
	ssh_client.close()
	return HttpResponse("done")

