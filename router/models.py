from django.db import models

# Create your models here.

class RouterInfo(models.Model):
	router = models.CharField(max_length=100)
	interface = models.CharField(max_length=100)
	description = models.TextField()
	bandwidth = models.IntegerField()
	ip_address = models.CharField(max_length=100)
	gateway = models.CharField(max_length=100)

	def __str__(self):
		return self.router