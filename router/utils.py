import os
import re
import logging
from pathlib import Path
from paramiko import SSHClient, AutoAddPolicy
from django.contrib import admin
from router.models import RouterInfo

logger = logging.getLogger('django')

def delete_router(router_data):
	logger.info('delete_router method')
	home = str(Path.home())
	localpath = home + '/test_file2.txt'
	'''read lines'''
	with open(localpath,"r") as f:
		contents = f.readlines()
		f.close()
	for position,line in enumerate(contents):
		if line.startswith("interface"):
			lnum = re.findall('\d+',line.split()[1])
			enum = re.findall('\d+',router_data.interface)
			if lnum:
				''' find which line start to delete '''
				if lnum==enum:
					for index in range(position,len(contents)):
							'''compare contents data with database '''
							if contents[index].rstrip("\n")=="interface"+" "+router_data.interface and contents[index+1].rstrip("\n")==" description"+" "+router_data.description and contents[index+2].rstrip("\n")==" bandwidth"+" "+str(router_data.bandwidth) and contents[index+3].rstrip("\n")==" ip address"+" "+router_data.ip_address+" "+router_data.gateway:
									'''if condition matched then delete it'''
									del contents[position:position+4]
									logger.info('delete router')
									break
	return contents

def save_router(router_dict):
	logger.info('save_router method')
	home = str(Path.home())
	localpath = home + '/test_file2.txt'
	interface = next(iter(router_dict.values()))
	elist = []
	with open(localpath,"r") as f:
		contents = f.readlines()
		f.close()
	'''to chunk line'''
	for position,line in enumerate(contents):
		if line.startswith("interface") or line.startswith("#"):
			lnum = re.findall('\d+',line.split()[1])
			elist.append(lnum)
			enum = re.findall('\d+',interface)
			if lnum:
				'''check interface condition if interface is less'''
				if lnum >= enum:
					elist.append(enum)
					try:
						count = 0
						while count<4:
							for i in router_dict.keys():
								'''to insert a space only first insertion(interface) operation'''
								if count>0:
									contents.insert(position+count,' {} {}\n'.format(i,router_dict[i]))
								else:
									contents.insert(position,'{} {}\n'.format(i,router_dict[i]))
								count+=1
							with open(localpath,"w") as f:
								contents = "".join(contents)
								f.write(contents)
								f.close()
							break
						break
					except:
						pass


	with open(localpath,"r") as f:
		contents = f.readlines()
		f.close()
	'''check interface condition if interface is greater'''
	if enum not in elist:
		for position,line in enumerate(contents):
			if line == 'end\n':
				try:
					count=0
					while count<4:
						for i in router_dict:
							'''to insert a space only first insertion(interface) operation'''
							if count>0:
								contents.insert(position-2+count,' {} {}\n'.format(i,router_dict[i]))
							else:
								contents.insert(position-2,'{} {}\n'.format(i,router_dict[i]))
							count+=1
						with open(localpath,"w") as f:
							contents = "".join(contents)
							f.write(contents)
							f.close()
						break
				except:
					pass
	return localpath
