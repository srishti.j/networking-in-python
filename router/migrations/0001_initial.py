# Generated by Django 2.0 on 2021-03-22 06:12

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='RouterInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('router', models.CharField(max_length=100)),
                ('interface', models.CharField(max_length=100)),
                ('description', models.TextField()),
                ('bandwidth', models.IntegerField()),
                ('ip_address', models.IntegerField()),
                ('gateway', models.IntegerField()),
            ],
        ),
    ]
