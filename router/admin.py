import os
import logging
from scp import SCPClient
from pathlib import Path
from paramiko import SSHClient, AutoAddPolicy
from django.contrib import admin
from router.models import RouterInfo
from router.forms import RouterInfoAdminForm
from router.utils import delete_router, save_router

logger = logging.getLogger('django')

# @admin.register(RouterInfo)
class RouterInfoAdmin(admin.ModelAdmin):

	list_display = ('router','interface')
	readonly_fields = ('id',)

	def save_model(self, request, obj, form, change):
		logger.info("save router")
		'''connect to the server'''
		ssh_client = SSHClient()
		ssh_client.set_missing_host_key_policy(AutoAddPolicy())
		ssh_client.connect(
					hostname='35.197.43.130',
					username='srishti',
					sock=None,
					key_filename='/home/ebabu/Downloads/test_box'
				)
		
		scp = SCPClient(ssh_client.get_transport())
		home = str(Path.home())
		localpath = home + '/test_file2.txt'
		remotepath = '/home/srishti/wan5.conf'
		scp.get(remotepath,localpath)
		logger.info('copy server text file into localpath')
		router_dict = {
						"interface":obj.interface,
						"description":obj.description,
						"bandwidth":str(obj.bandwidth),
						"ip address":obj.ip_address+' '+obj.gateway,
						}
		''' to create and save new data '''
		if not change:
			localpath = save_router(router_dict)
			''' put updated data into server file'''
			path = '/home/srishti/wan5.conf'
			logger.info('upload localpath file to the server file')
			scp.put(localpath,path)
			scp.close()
			logger.info('close ssh connection')
			ssh_client.close()
			return super(RouterInfoAdmin, self).save_model(request, obj, form, change)

		''' to update data '''
		if change:
			router_data=RouterInfo.objects.get(id=obj.id)
			contents = delete_router(router_data)
			''' update file '''
			with open(localpath,"w") as f:
				contents = "".join(contents)
				f.write(contents)
				f.close()
			localpath = save_router(router_dict)
			form = RouterInfoAdminForm(request.POST,instance=router_data)
			'''save new data into database'''
			form.save()
			logger.info('save form data into database')
			''' put updated data into server file'''
			path = '/home/srishti/wan5.conf'
			scp.put(localpath,path)	
			scp.close()
			logger.info('close scp connection')
			ssh_client.close()
			return super(RouterInfoAdmin, self).save_model(request, obj, form, change)

	''' delete interface '''
	def delete_view(self,request,object_id,extra_context=None):
		logger.info('delete router')
		router_data = RouterInfo.objects.get(id=object_id)
		'''connect to server'''
		logger.info('connect to server')
		ssh_client = SSHClient()
		ssh_client.set_missing_host_key_policy(AutoAddPolicy())
		ssh_client.connect(
					hostname = '35.197.43.130',
					username = 'srishti',
					sock = None,
					key_filename = '/home/ebabu/Downloads/test_box'
				)
		
		scp = SCPClient(ssh_client.get_transport())
		home = str(Path.home())
		localpath = home + '/test_file2.txt'
		remotepath = '/home/srishti/wan5.conf'
		scp.get(remotepath,localpath)
		contents = delete_router(router_data)
		with open(localpath,"w") as f:
			contents = "".join(contents)
			f.write(contents)
			f.close()

		path = '/home/srishti/wan5.conf'
		scp.put(localpath,path)	
		scp.close()
		ssh_client.close()
		return super(RouterInfoAdmin,self).delete_view(request,object_id,extra_context)

admin.site.register(RouterInfo,RouterInfoAdmin)
	
